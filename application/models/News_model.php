<?php
class News_model extends CI_Model{
	function getAll(){
		$keyword = $this->input->get('keyword');
		$this->db->like(array('title'=>$keyword));
		return $this->db->get('news')->result();
	}
}
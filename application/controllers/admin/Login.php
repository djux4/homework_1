<?php
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('admin'))
			redirect('admin/success');
	}
	function index(){
		$this->load->view('admin/login');
	}
	function verify(){
		$this->load->model('admin');
		$check = $this->admin->validate();
		if($check){
			$this->session->set_userdata('admin','1');
			redirect('admin/success');

		}else{
			redirect('admin');
		}
		
	}
}